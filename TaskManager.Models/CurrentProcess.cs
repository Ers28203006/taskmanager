﻿namespace TaskManager.Models
{
    public class CurrentProcess
    {
        public string ProcessName { get; set; }
        public int ProcessId { get; set; }
        public bool ProcessStatus { get; set; }
        public string UserName { get; set; }
        public string CPU { get; set; }
        public long Memory { get; set; }
        public string Description { get; set; }
    }
}
