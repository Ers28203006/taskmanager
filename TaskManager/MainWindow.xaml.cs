﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Security.Principal;
using System.Windows;
using TaskManager.Models;

namespace TaskManager
{
    public partial class MainWindow : Window
    {
        private ObservableCollection<CurrentProcess> currentProcesses = null;
        private Process[] currentProc = null;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void WindowLoaded(object sender, RoutedEventArgs e)
        {
            currentProcesses = new ObservableCollection<CurrentProcess>();

            currentProc = Process.GetProcesses();
            foreach (var process in currentProc)
            {
                try
                {
                    currentProcesses.Add
                    (
                        new CurrentProcess
                        {
                            ProcessName = process.ProcessName,
                            ProcessId = process.Id,
                            ProcessStatus = process.Responding,
                            Memory = process.PagedSystemMemorySize64,
                            UserName = WindowsIdentity.GetCurrent().Name,
                            Description = process.MainModule.FileVersionInfo.FileDescription,
                        }
                    );
                }
                catch (System.ComponentModel.Win32Exception exeption)
                {
                    currentProcesses.Add
                    (
                        new CurrentProcess
                        {
                            ProcessName = process.ProcessName,
                            ProcessId = process.Id,
                            ProcessStatus = process.Responding,
                            Memory = process.PagedSystemMemorySize64,
                            UserName = WindowsIdentity.GetCurrent().Name,
                            Description = exeption.Message,
                        }
                    );
                }
                catch(InvalidOperationException exeption)
                {
                    MessageBox.Show(exeption.Message);
                }

            }

            for (int i = 0; i < currentProcesses.Count(); i++)
            {
                try
                {
                    currentProcesses[i].CPU = currentProc[i].TotalProcessorTime.Minutes.ToString();
                }

                catch (System.ComponentModel.Win32Exception exeption)
                {
                    currentProcesses[i].CPU = exeption.Message;
                }

                catch (InvalidOperationException exeption)
                {
                    MessageBox.Show(exeption.Message);
                }
            }

            dataGrid.ItemsSource = currentProcesses;
        }

        private void RemoveTaskButtonClick(object sender, RoutedEventArgs e)
        {
            CurrentProcess process = new CurrentProcess();
            process = ((CurrentProcess)dataGrid.SelectedItem);

            try
            {
                foreach (var proc in currentProc)
                    if (proc.Id == process.ProcessId)
                    {
                        proc.Kill();
                        currentProcesses.Remove((CurrentProcess)dataGrid.SelectedItem);
                        break;
                    }
            }
            catch (System.ComponentModel.Win32Exception exeption)
            {
                MessageBox.Show(exeption.Message);
            }

            catch (InvalidOperationException exeption)
            {
                MessageBox.Show(exeption.Message);
            }

            catch (NotSupportedException exeption)
            {
                MessageBox.Show(exeption.Message);
            }
        }
    }
}
